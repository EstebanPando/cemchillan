from django import forms
from .modelsInv import RegistroInv

class RegModelForm(forms.ModelForm):
	
    def clean_nombre(self):
        nombre=self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) >= 2 and len(nombre) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return nombre
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes")

    def clean_cantidad(self):
        cantidad = self.cleaned_data.get("cantidad")
        if cantidad:
            if cantidad == 0:
                raise forms.ValidationError("No se puede ingresar valores en 0")
            else:
                return cantidad