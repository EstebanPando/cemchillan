from django.db import models

# Create your models here.
class RegistroInv(models.Model):
	identificador = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	cantidad = models.PositiveIntegerField()
	pertenencia = models.CharField(max_length=100, null=False)
	pertenenciasec = models.CharField(max_length=100, null=False)
	def __str__(self):
		return self.nombre