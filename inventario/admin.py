from django.contrib import admin
from .modelsInv import *
from .formsInv import RegModelForm
# Register your models here.

class AdminRegistroInv (admin.ModelAdmin):
	list_display = ["nombre", "cantidad", "pertenencia", "pertenenciasec"]
	form = RegModelForm
	list_filter = ["nombre"]
	list_editable = ["pertenenciasec"]
	search_fields = ["nombre", "pertenencia"]

admin.site.register(RegistroInv, AdminRegistroInv)