from django.db import models

# Create your models here.

class Registro(models.Model):
	identificador = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	descripcion = models.CharField(max_length=100, blank=True, null=True)
	categoria = models.CharField(max_length=100, blank=True, null=True)
	subgrupo = models.CharField(max_length=100, blank=True, null=
		True)
	def __str__(self):
		return self.nombre

