from django.shortcuts import render
from .forms import RegForm
from .models import Registro
# Create your views here.

def inicio(request):
	form = RegForm(request.POST or None)
	if form.is_valid():
		form_data = form.cleaned_data
		nombre2 = form_data.get("nombre")
		descripcion2 = form_data.get("descripcion")
		categoria2 = form_data.get("categoria")
		subgrupo2 = form_data.get("subgrupo")
		objeto = Registro.objects.create(nombre=nombre2, descripcion=descripcion2, categoria=categoria2, subgrupo=subgrupo2)

	contexto = {
	"formulario_producto":form,
	}
	return render(request, 'inicio.html', contexto)