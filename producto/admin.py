from django.contrib import admin
from producto.models import *
from .forms import RegForm, RegModelForm
# Register your models here.

class AdminRegistro (admin.ModelAdmin):
	list_display = ["nombre", "descripcion", "categoria", "subgrupo"]
	form = RegModelForm
	list_filter = ["nombre"]
	list_editable = ["subgrupo"]
	search_fields = ["nombre", "categoria"]

admin.site.register(Registro, AdminRegistro)


