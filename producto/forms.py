from django import forms
from .models import Registro

class RegModelForm(forms.ModelForm):
	class Meta:
		modelo = Registro
		campos = ["nombre", "descripcion", "categoria", "subgrupo"]

class RegForm(forms.Form):
	nombre = forms.CharField(max_length=100)
	descripcion = forms.CharField(max_length=100)
	categoria = forms.CharField(max_length=100)
	subgrupo = forms.CharField(max_length=100)