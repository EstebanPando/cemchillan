from django.db import models

# Create your models here.
class RegistroAdq(models.Model):
	identificador = models.AutoField(primary_key=True, unique=True)
	nombreproducto = models.CharField(max_length=100,blank=True,null=True)
	nombreproveedor = models.CharField(max_length=100,blank=True,null=True)
	cantidad = models.CharField(max_length=100,blank=True,null=True)
	fecha = models.DateTimeField(auto_now_add=True, auto_now=False)
	factura = models.CharField(max_length=100, blank=True, null=True)
	def __str__(self):
		return self.nombreproducto
