from django import forms
from .modelsAdq import RegistroAdq

class RegModelForm(forms.ModelForm):
	class Meta:
		modelo = RegistroAdq
		campos = ["nombreproducto", "nombreproveedor", "cantidad", "fecha", "factura"]

class RegForm(forms.Form):
	nombreproducto = forms.CharField(max_length=100)
	nombreproveedor = forms.CharField(max_length=100)
	cantidad = forms.CharField(max_length=100)
	fecha = forms.DateTimeField()
	factura = forms.CharField(max_length=100)