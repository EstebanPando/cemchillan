from django.contrib import admin
from .modelsAdq import *
from .formsAdq import RegForm, RegModelForm
# Register your models here.

class AdminRegistroAdq (admin.ModelAdmin):
	list_display = ["nombreproducto", "nombreproveedor", "cantidad", "fecha", "factura"]
	form = RegModelForm
	list_filter = ["nombreproducto"]
	list_editable = ["cantidad"]
	search_fields = ["nombreproducto", "nombreproveedor"]

admin.site.register(RegistroAdq, AdminRegistroAdq)