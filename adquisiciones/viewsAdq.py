from django.shortcuts import render
from .formsAdq import RegForm
from .modelsAdq import RegistroAdq

# Create your views here.
def inicio1(request):
	form = RegForm(request.POST or None)
	if form.is_valid():
		form_data = form.cleaned_data
		nombreproducto2 = form_data.get("nombre")
		nombreproveedor2 = form_data.get("descripcion")
		cantidad2 = form_data.get("categoria")
		fecha2 = form_data.get("subgrupo")
		factura2 = form_data.get("factura")
		objeto = RegistroAdq.objects.create(nombreproducto=nombreproducto2, nombreproveedor=nombreproveedor2, cantidad=cantidad2, fecha=fecha2, factura=factura2)

	contexto = {
	"formulario_adquisicion":form,
	}
	return render(request, 'inicio1.html', contexto)