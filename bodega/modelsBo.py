from django.db import models

# Create your models here.
class RegistroBo(models.Model):
	identificador = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	stock = models.IntegerField(blank=True, null=True)
	stockminimo = models.PositiveIntegerField(default=1)
	ubicacion = models.CharField(max_length=100, null=False, default=1)
	fecha = models.DateTimeField(auto_now_add=True, auto_now=False)
	def __str__(self):
		return self.nombre







#anno = models.IntegerField(blank=True, null=True, verbose_name="Año de Emision de la Resolucion")