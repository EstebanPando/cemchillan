from django.contrib import admin
from .modelsBo import *
from .formsBo import RegModelForm
# Register your models here.

class AdminRegistroBo (admin.ModelAdmin):
	list_display = ["nombre", "stock", "stockminimo", "ubicacion", "fecha"]
	form = RegModelForm
	list_filter = ["nombre"]
	list_editable = ["ubicacion"]
	search_fields = ["nombre", "stock"]

admin.site.register(RegistroBo, AdminRegistroBo)
