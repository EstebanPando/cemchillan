from django import forms
from .modelsBo import RegistroBo

class RegModelForm(forms.ModelForm):
	
    def clean_nombre(self):
        nombre=self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) >= 2 and len(nombre) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return nombre
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes (Mínimo 2)")

    def clean_ubicacion(self):
        ubicacion=self.cleaned_data.get("ubicacion")
        if ubicacion: # Validamos que el contenido del campo nombre no venga vacío
            if len(ubicacion) >= 2 and len(ubicacion) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo ubicacion no puede ser de un caracter")
                return ubicacion
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes (Mínimo 2)")


    def clean_stockminimo(self):
        stockminimo = self.cleaned_data.get("stockminimo")
        if stockminimo:
            if stockminimo == 0:
                raise forms.ValidationError("No se puede ingresar valores en 0")
            else:
                return stockminimo